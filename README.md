# autonomous_robot
A project for our University about an autonomous robot/car

&nbsp;

<div align = "center">

![Repository Size][rep-size-img] &nbsp; ![Code Size][code-size-img] &nbsp; ![Code File Size][code-file-img] &nbsp; ![Lines of Code][code-lines-img]

</div>


[code-size-img]:    https://img.shields.io/github/languages/code-size/FenFr/autonomous_robot?label=Code%20Size&style=flat-square
[code-file-img]:    https://img.shields.io/github/directory-file-count/fenfr/autonomous_robot?label=Files&style=flat-square
[code-lines-img]:   https://img.shields.io/tokei/lines/github/fenfr/autonomous_robot?label=Lines%20of%20Code&style=flat-square
[rep-size-img]:     https://img.shields.io/github/repo-size/FenFr/autonomous_robot?label=Repo%20Size&style=flat-square
